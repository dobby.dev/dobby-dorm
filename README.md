| **Development**                                                                                                                             | **Staging**                                                                                                                                     | **Stable**                                                                                                                              |
| ------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------- |
| [![Netlify Status](https://api.netlify.com/api/v1/badges/8a638c3c-056a-40bb-8fbd-07ce161e3e5f/deploy-status)](https://dorm-dev.netlify.app) | [![Netlify Status](https://api.netlify.com/api/v1/badges/31d4ad6f-fdba-435f-9d64-ba1dbdf32e22/deploy-status)](https://dorm-staging.netlify.app) | [![Netlify Status](https://api.netlify.com/api/v1/badges/d2fe5274-6185-4930-b763-ef0cf6a64e99/deploy-status)](https://dorm.netlify.app) |

# DOBBY DORM

---

> el LocalStorage ORM

---

### Commands & Setups

-   Testing: `npm run test`
-   Lint: `npm run lint-fix`
-   Develop & Watch: `npm run dev`

### Push & Lint

-   Copy contents of `pre-push.hook.example` file to:
    -   `.git/hooks/pre-push`
    -   Configure file before saving.

> This automates your git pushes to make sure lint & testing is done before push.

### Dependencies

> added 608 packages, and audited 609 packages.
