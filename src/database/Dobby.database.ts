import { DobbyStorageClass } from "../classes/storage/DobbyStorage.class";
import {
    dbFingerprintLsKey,
    generateDbFingerprint,
} from "../utils/database.utils";
import DobbyTalks from "../../node_modules/dobby-talks/src/index";
import { FingerprintNotDetected } from "../errors/DatabaseError";
import { downloadDB } from "../utils/download.utils";

/**
 * NOTE:
 * Handles Database usage.
 */
export class DobbyDatabase extends DobbyStorageClass {
    fingerprint = generateDbFingerprint();
    fingerprintKey = dbFingerprintLsKey();
    _l = new DobbyTalks();

    constructor() {
        super();

        this.handshakeFingerprint();
    }

    /**
     * NOTE:
     * returns current fingerprint or throws
     *
     */
    public readFingerprint(): string {
        const fingerprint = window.localStorage.getItem(this.fingerprintKey);
        if (fingerprint === null) {
            throw new FingerprintNotDetected();
        }

        return fingerprint;
    }

    /**
     * NOTE:
     * Alias from private printSizeNotice()
     * Logs information about space used by the DB.
     *
     * @alias printSizeNotice();
     */
    public size(): void {
        this.printSizeNotice();
    }

    /**
     * NOTE:
     * Downloads database file
     */
    public backup(fileName = "dDORM-backup"): void {
        downloadDB(fileName);
    }

    /*******************************************************************************************************************
     * PRIVATE METHODS
     */

    /**
     * NOTE:
     * Logs information about space used by the DB.
     */
    private printSizeNotice(): void {
        let outputNotice = `Total Size: ~ ${this.totalSpaceString}\n`;
        outputNotice += `Free Size:  ~ ${this.freeSpaceString}\n\n`;
        outputNotice += `Used Size:  ~ ${this.usedSpaceString}`;

        this._l.talk(outputNotice, "DB QUOTA INFORMATION", false);
    }

    /**
     * NOTE:
     * Compares stored fingerprint with system version fingerprint.
     * @private
     */
    private handshakeFingerprint(): void {
        const currentVersion = window.localStorage.getItem(this.fingerprintKey);

        if (currentVersion === null) {
            // no version detected
            this._l.talk("no version", "DB", false);
            this.updateFingerprint();
        } else if (currentVersion === this.fingerprint) {
            // same version detected
            this._l.talk(`same version`, `${this.fingerprint}`);
        } else {
            // different version detected
            this.upgradeFingerprint();
        }
    }

    /**
     * NOTE:
     * Simply update current key.
     * Useful for new entries or resets.
     * @private
     */
    private updateFingerprint(): void {
        window.localStorage.setItem(this.fingerprintKey, this.fingerprint);
        this._l.talk(`version updated to ${this.fingerprint}`, "DB", false);
    }

    /**
     * NOTE:
     * Upgrade version data from previous fingerprint into new fingerprint.
     * @TODO => migrate data
     * @private
     */
    private upgradeFingerprint(): void {
        window.localStorage.setItem(this.fingerprintKey, this.fingerprint);
        this._l.talk("version updated!", `DB UPDATED TO: ${this.fingerprint}`);
    }
}
