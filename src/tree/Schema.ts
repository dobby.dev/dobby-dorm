import { Logger } from "./Logger";
import type { ModelSchemaClass } from "../classes/models/ModelSchema.class";
import { NoModelSchemaFoundForName } from "../errors/ModelSchemaErrors";
import { dbSchemaKey } from "../utils/database.utils";
import { md5 } from "../utils/hash.utils";
import {
    SchemaHashNotDetected,
    SchemaModelNamesNotDetected,
} from "../errors/DatabaseError";

/**
 * Appends Model Schema and computational necessities.
 */
export class Schema extends Logger {
    models: ModelSchemaClass[];
    modelNames: string;
    schemaKey = dbSchemaKey();
    schemaNamesKey = `${dbSchemaKey()}-names`;

    constructor(schemaList: ModelSchemaClass[]) {
        super();

        this.models = schemaList;
        this.modelNames = this.models.map((model) => model.name).join("||");

        this.handshakeSchema();
    }

    /**
     * NOTE:
     * Gets ModelSchema by given name.
     *
     * @param modelName
     * @return model<ModelSchemaClass>
     */
    getSchemaByName(modelName): ModelSchemaClass {
        const modelSchemaClass = this.models.find((model) => {
            return model.name === modelName;
        });
        if (modelSchemaClass === undefined) {
            throw new NoModelSchemaFoundForName(modelName);
        }

        return modelSchemaClass;
    }

    /**
     * NOTE:
     * Prints stored schemaHash
     */
    public readSchemaHash(): string {
        const hash = window.localStorage.getItem(this.schemaKey);
        if (hash === null) {
            throw new SchemaHashNotDetected();
        }

        return hash;
    }

    /**
     * NOTE:
     * Prints stored model names.
     */
    public readModelNames(): string {
        const names = window.localStorage.getItem(this.schemaNamesKey);
        if (names === null) {
            throw new SchemaModelNamesNotDetected();
        }

        return names;
    }

    /*******************************************************************************************************************
     * Private methods
     */

    /**
     * NOTE:
     * Validates & matches schema.
     * Updates or Writes for the first time.
     *
     * @private
     */
    private handshakeSchema(): void {
        const jsonSchema = md5(JSON.stringify(this.models));
        const currentSchema = window.localStorage.getItem(this.schemaKey);
        if (currentSchema === null) {
            this.writeSchema(jsonSchema);
        } else if (currentSchema !== jsonSchema) {
            this.updateSchema(jsonSchema);
        }
    }

    /**
     * NOTE:
     * Writes schema for the first time.
     * Writes schema model names
     *
     * @param jsonSchema
     * @private
     */
    private writeSchema(jsonSchema: string): void {
        this.logger.talk(jsonSchema, `DB SCHEMA WRITTEN`);
        window.localStorage.setItem(this.schemaKey, jsonSchema);
        window.localStorage.setItem(this.schemaNamesKey, this.modelNames);

        this.models.forEach((model) => {
            window.localStorage.setItem(
                model.schemaKey,
                JSON.stringify(model.attributes)
            );
        });
    }

    /**
     * NOTE:
     * Updates schema content. Including data.
     * Updates schema model names
     *
     * @param jsonSchema
     * @private
     */
    private updateSchema(jsonSchema: string): void {
        this.logger.talk(jsonSchema, `DB SCHEMA UPDATED`);
        window.localStorage.setItem(this.schemaKey, jsonSchema);
        window.localStorage.setItem(this.schemaNamesKey, this.modelNames);

        this.models.forEach((model) => {
            window.localStorage.setItem(
                model.schemaKey,
                JSON.stringify(model.attributes)
            );
        });
    }
}
