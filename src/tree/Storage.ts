import { DobbyDatabase } from "../database/Dobby.database";

/**
 * NOTE:
 * Storage injection on DORM.
 */
export class Storage extends DobbyDatabase {}
