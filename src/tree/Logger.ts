import DobbyTalks from "../../node_modules/dobby-talks/src/index";
import { Storage } from "./Storage";
import { version as v } from "../../dist/package-info.json";

/**
 * NOTE:
 * Appends Log functionality to parent class
 */
export class Logger extends Storage {
    logger: DobbyTalks;
    version = v.toString();
    constructor() {
        super();

        this.logger = new DobbyTalks();
        this.printDisclaimer();
    }

    /*********************************************************************************************************************
     * PRIVATE METHODS
     */

    /**
     * NOTE:
     * Highlights the disclaimer message.
     * @private
     */
    private printDisclaimer(): void {
        this.logger.highlight(getDisclaimerMessage(this), false);
    }
}

/**
 * NOTE:
 * Local method to retrieve disclaimerMessage.
 *
 * @local
 */
function getDisclaimerMessage(logger: Logger): string {
    let out = ` Welcome to Dobby DORM. (v${
        logger.version as string
    })\n      Your magical LocalStorage ORM !!\n`;
    out += "\n";
    out += "      Total Space: ~ " + logger.totalSpaceString + " \n";
    out += "      Free Space:  ~ " + logger.freeSpaceString + " \n";
    out += "      Used Space:  ~ " + logger.usedSpaceString + " ";

    return out;
}
