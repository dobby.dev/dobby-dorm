export function downloadDB(fileName = "dDORM-backup"): void {
    const dataBlob = window.localStorage;
    const indentationSpace = 2;
    const dataStr =
        "data:text/json;charset=utf-8," +
        encodeURIComponent(JSON.stringify(dataBlob, null, indentationSpace));

    const body = document.getElementsByTagName("body")[0];
    const el = document.createElement("div");
    el.innerHTML =
        '<div><a id="downloadAnchorElem" style="display:none"></a></div>';

    body.appendChild(el);

    const dlAnchorElem = document.getElementById("downloadAnchorElem");

    if (dlAnchorElem === null) {
        return;
    }

    dlAnchorElem.setAttribute("href", dataStr);
    dlAnchorElem.setAttribute("download", `${fileName}.json`);
    dlAnchorElem.click();
}
