import CryptoES from "../../node_modules/crypto-es/lib/index";
import v4 from "../../node_modules/uuid/dist/esm-browser/v4";
export function md5(input: string): string {
    return CryptoES.MD5(input).toString();
}

/**
 * NOTE:
 * Generates random unique id.
 */
export function genUid(): string {
    return v4();
}
