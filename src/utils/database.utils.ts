import { env } from "../../env.environment";

/**
 * NOTE:
 * Returns standardized fingerprint for database.
 */
export function generateDbFingerprint(): string {
    return `dDORM-${env.DB_VERSION}`;
}

/**
 * NOTE:
 * Returns the key to store DB version.
 */
export function dbFingerprintLsKey(): string {
    return `dDORM-DB-VERSION`;
}

/**
 * NOTE:
 * Returns the key used to check and handshake application schema.
 */
export function dbSchemaKey(): string {
    return `dDORM-DB-SCHEMA-${env.DB_VERSION}`;
}
