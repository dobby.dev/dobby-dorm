import DobbyTalks from "../../node_modules/dobby-talks/src/index";

const oneSecond = 1000;

export class BaseWorker {
    timer: number;
    interval?: IntervalType;
    callback?: any;
    _l = new DobbyTalks();
    constructor(callBack: any, timerMs = oneSecond) {
        this.timer = timerMs;
        this.callback = callBack;

        this.startInterval();
    }

    /**
     * NOTE:
     * Logs a ping on call.
     */
    public ping(): void {
        this._l.h(`${this.constructor.name} Ping!!`);
    }

    /*********************************************************************************************************************
     * PRIVATE METHODS
     */

    /**
     * NOTE:
     * Starts interval and stores it on memory.
     *
     * @private
     */
    private startInterval(): void {
        if (this.interval !== undefined) {
            clearInterval(this.interval);
        } else {
            const delayDisclaimer = 666;
            setTimeout(() => {
                this._l.h(`${this.constructor.name} enabled`);
                this.interval = setInterval(() => {
                    this.callback();
                }, this.timer);
            }, delayDisclaimer);
        }
    }
}
