/**
 * NOTE:
 * Contains ModelAttribute type, name and can contain value.
 * Sustains computational necessities of class.
 */
export class ModelAttributeClass {
    name: string;
    type: string;
    value?: ModelAttributeValueType;

    /**
     * NOTE:
     * Returns a list with invalid model attribute names.
     * Keywords reserved for system.
     */
    static invalidAttributeNames(): string[] {
        return ["id", "type", "created_at", "updated_at"];
    }

    constructor(name: string, type: string, value?: ModelAttributeValueType) {
        this.name = name;
        this.type = type;
        this.value = value;
    }

    /**
     * NOTE:
     * Sets value for attribute from model.
     *
     * stores
     * @param value
     */
    public setValue(value: ModelAttributeValueType): void {
        window.localStorage.setItem(
            `db-${this.name}-entry`,
            JSON.stringify(value)
        );
    }
}
