import { DobbyDatabase } from "../../database/Dobby.database";
import { ModelClass } from "./Model.class";
import {
    IdNotDefinedError,
    NameNotDefinedError,
} from "../../errors/ModelCrudError";
import { dbSchemaKey } from "../../utils/database.utils";

export class ModelAbstractClass extends DobbyDatabase {
    id: string | null;
    schemaKey = `${dbSchemaKey()}-schema`;
    name?: string;
    any: string;

    static find(modelName: string, idString: string): ModelClass | null {
        const key = `${dbSchemaKey()}-schema--${modelName}__${idString}-id`;

        let id = window.localStorage.getItem(key);

        if (id === null) {
            return null;
        }

        try {
            id = JSON.parse(id);
        } catch (err) {
            return null;
        }

        return new ModelClass(modelName, id as string);
    }

    /**
     * NOTE:
     * Gets attribute by name
     *
     * @todo throw validation
     * @param attributeName
     */
    public get(attributeName: ModelAttributeNameType): ModelAttributeValueType {
        if (this.id === null) {
            throw new IdNotDefinedError();
        }

        return window.localStorage.getItem(
            this.modelAttributeKey(attributeName)
        );
    }

    /**
     * NOTE:
     * Sets attribute value by name and value.
     *
     * @todo throw validation
     * @param attributeName
     * @param value
     */
    public set(
        attributeName: ModelAttributeNameType,
        value: ModelAttributeValueType
    ): ModelAbstractClass {
        window.localStorage.setItem(
            this.modelAttributeKey(attributeName),
            JSON.stringify(value)
        );

        return this;
    }

    /*********************************************************************************************************************
     * PRIVATE METHODS
     */

    private modelAttributeKey(attributeName: string): string {
        if (this.id === null) {
            throw new IdNotDefinedError();
        }

        if (this.name === undefined) {
            throw new NameNotDefinedError();
        }

        return `${this.schemaKey}__${this.id}-${attributeName}`;
    }
}
