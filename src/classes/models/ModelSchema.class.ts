import { ModelAttributeClass } from "./ModelAttribute.class";
import { ReservedKeywordUsedAsAttribute } from "../../errors/ModelSchemaErrors";
import { dbSchemaKey } from "../../utils/database.utils";
import DobbyTalks from "../../../node_modules/dobby-talks/src/index";
import { SchemaNotDetected } from "../../errors/DatabaseError";

/**
 * NOTE:
 * Sustains model name and attributes.
 * Attribute list contains attribute type and name. Can also contain value.
 */
export class ModelSchemaClass {
    name: string;
    attributes: ModelAttributeClass[];
    schemaKey = `${dbSchemaKey()}-schema`;
    _l = new DobbyTalks();
    storage = window.localStorage;

    constructor(name: string, attributes: ModelAttributeType[]) {
        this.name = name;
        this.schemaKey += `--${this.name}`;

        this.validateAttributeNames(attributes);
        this.setupAttrs(attributes);
    }

    /**
     * NOTE:
     * Prints current modelSchema string.
     */
    public readSchema(): string {
        const schema = this.storage.getItem(this.schemaKey);
        if (schema === null) {
            throw new SchemaNotDetected();
        }

        return schema;
    }

    /*********************************************************************************************************************
     * PRIVATE METHODS
     */

    /**
     * NOTE:
     * Hydrate attribute types.
     *
     * @param attrs
     * @private
     */
    private setupAttrs(attrs: ModelAttributeType[]): void {
        this.attributes = [];

        attrs.forEach((at) => {
            this.attributes.push(
                new ModelAttributeClass(at.name, at.type, null)
            );
        });
    }

    /**
     * NOTE:
     * Validates Attribute names.
     *
     * @param attrs
     * @private
     */
    private validateAttributeNames(attrs: ModelAttributeType[]): void {
        let invalidName = "";
        let invalidDetected = false;
        const givenAttrNames = attrs.map(
            (att: ModelAttributeClass) => att.name
        );
        const reservedKeywords = ModelAttributeClass.invalidAttributeNames();

        // loop given attribute names
        givenAttrNames.forEach((givenName) => {
            // finds and validates index less than zero (no found)
            const checkIndex = reservedKeywords.findIndex((reservedKey) => {
                return reservedKey === givenName;
            });
            if (checkIndex >= 0) {
                invalidDetected = true;
                invalidName = reservedKeywords[checkIndex];
            }
        });

        // if found throws error.
        if (invalidDetected) {
            throw new ReservedKeywordUsedAsAttribute(invalidName);
        }
    }
}
