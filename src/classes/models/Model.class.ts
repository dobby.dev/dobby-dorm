import { SchemaNotDetected } from "../../errors/DatabaseError";
import { genUid } from "../../utils/hash.utils";
import { ModelAbstractClass } from "./ModelAbstract.class";

/**
 * NOTE:
 * Handles Model properties.
 */
export class ModelClass extends ModelAbstractClass {
    attributes: ModelSchemaType[];
    values: ModelAttributeValueType[];
    data: { id; any };
    schema: ModelSchemaType[];

    /**
     * NOTE:
     * Class constructor.
     *
     * @param name
     * @param id
     */
    constructor(name: string, id = "") {
        super();

        this.name = name;
        this.schemaKey += `--${this.name}`;
        this.id = id.length > 0 ? id : genUid();

        this.loadSchema();
        this.loadAbstractionLayer();
        this.set("id", id);
    }

    /*********************************************************************************************************************
     * PRIVATE METHODS
     */

    /**
     * NOTE:
     * Loads schema from model name
     *
     * @private
     * @throws SchemaNotDetected when no schema can be read
     */
    private loadSchema(): void {
        const schema = window.localStorage.getItem(this.schemaKey);
        if (schema === null) {
            throw new SchemaNotDetected();
        }

        this.schema = JSON.parse(schema);

        this.schema.push({
            name: "id",
            type: "string",
        });

        this.set("id", this.id);
        this.attributes = this.schema;
    }

    /**
     * NOTE:
     * loads object in memory.
     *
     * @private
     */
    private loadAbstractionLayer(): void {
        this.attributes.forEach((att) => {
            if (att.name === "id") {
                return;
            }

            this[att.name] = this.get(att.name);
        });
    }
}
