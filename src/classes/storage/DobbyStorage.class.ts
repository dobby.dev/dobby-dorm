import { DobbyFreeStorageClass } from "./DobbyFreeStorage.class";

/**
 * NOTE:
 * Handles Storage complexity.
 */
export class DobbyStorageClass extends DobbyFreeStorageClass {
    units = ["bytes", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"];
    freeSpaceString: string;
    usedSpaceString: string;
    totalSpaceString: string;

    constructor() {
        super();

        this.freeSpaceString = this.sizeString(this.freeStorage);
        this.usedSpaceString = this.sizeString(this.usedStorage);
        this.totalSpaceString = this.sizeString(this.totalStorage);
    }

    public sizeString(size: number): string {
        const x = size.toString();
        let l = 0;
        let n = parseInt(x, 10) !== 0 ? parseInt(x, 10) : 0;

        while (n >= 1024 && ++l !== 0) {
            n = n / 1024;
        }

        return n.toFixed(n < 10 && l > 0 ? 1 : 0) + " " + this.units[l];
    }
}
