import { InvalidNumber } from "../../errors/DataTypeErrors";

/**
 * NOTE:
 * Handles free space calculations and predictions.
 */
export class DobbyFreeStorageClass {
    freeStorage: number;
    totalStorage: number;
    usedStorage: number;

    constructor() {
        this.calculateStorageValues();
    }

    /*********************************************************************************************************************
     * PRIVATE METHODS
     */

    /**
     * NOTE:
     * Chain actions to fill:
     * - .freeStorage <number>
     * - .totalStorage <number>
     * - .usedStorage <number>
     * @private
     */
    private calculateStorageValues(): void {
        this.calculateTotalStorage();
        this.calculateUsedStorage();
        this.calculateFreeStorage();
    }

    /**
     * NOTE:
     * Speculates available totalStorage.
     *
     * @private
     */
    private calculateTotalStorage(): void {
        const maxInMb = 4.5;
        const mb = 1024;
        this.totalStorage = Number(maxInMb * mb * mb);
    }

    /**
     * NOTE:
     * Calculates approx measurable used storage.
     *
     * @private
     */
    private calculateUsedStorage(): void {
        this.usedStorage = decodeURI(
            encodeURIComponent(JSON.stringify(localStorage))
        ).length;
    }

    /**
     * NOTE:
     * Calculates predictable free storage.
     * Updates .freeStorage property.
     *
     * @private
     */
    private calculateFreeStorage(): void {
        let freeStorage;
        try {
            freeStorage = Number(this.totalStorage - this.usedStorage);
        } catch (err) {
            throw new InvalidNumber();
        }

        this.freeStorage = freeStorage;
    }
}
