import { ModelSchemaClass } from "./classes/models/ModelSchema.class";
import { ModelClass } from "./classes/models/Model.class";
import { Schema } from "./tree/Schema";
import { ServiceWorker } from "./workers/service.worker";
import { FasterServiceWorker } from "./workers/FasterService.worker";
import { env } from "../env.environment";

const timerWorker = env.SERVICE_WORKER_TIMER;
const timerFasterWorker = env.FASTER_SERVICE_WORKER_TIMER;

/**
 * NOTE:
 * Entry point for DobbyDorm functionality.
 */
class DobbyDorm extends Schema {
    constructor(args) {
        super(args);

        // eslint-disable-next-line
        // @ts-ignore
        window.db = this;

        this.setupWorkers();
    }

    /*******************************************************************************************************************
     * PRIVATE METHODS
     */

    /**
     * NOTE:
     * Integrates different Workers on startup.
     *
     * @private
     */
    private setupWorkers(): void {
        const serviceWorker = new ServiceWorker(() => {
            serviceWorker.ping();
        }, timerWorker);

        const fasterServiceWorker = new FasterServiceWorker(() => {
            fasterServiceWorker.ping();
        }, timerFasterWorker);
    }
}

export { DobbyDorm, ModelSchemaClass, ModelClass };
