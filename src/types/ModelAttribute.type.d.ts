declare type ModelAttributeType = {
    name: string;
    type: string;
};
