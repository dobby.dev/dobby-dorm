/**
 * Sustains schema for ModelAttributeValue.
 * Any value that can be assigned to any model attribute.
 */
declare type ModelAttributeValueType =
    | number
    | string
    | Date
    | any[]
    | { T }
    | boolean
    | null;
