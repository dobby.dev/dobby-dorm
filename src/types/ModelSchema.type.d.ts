declare type ModelSchemaType = {
    name: string;
    type: string;
    value?: any;
};
