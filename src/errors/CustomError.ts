/**
 * NOTE:
 * @TODO => give Error suffix to child classes.
 *
 * Implementation for CustomError override on error name and message spacing.
 */
export class CustomError extends Error {
    constructor(args) {
        const argsAsString = args as string;
        const string = `\n\n${argsAsString}\n`;

        super(string);

        this.name = this.constructor.name;
    }
}
