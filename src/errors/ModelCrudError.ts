import { CustomError } from "./CustomError";

export class IdNotDefinedError extends CustomError {
    constructor() {
        super(`ID not defined!`);
    }
}

export class NameNotDefinedError extends CustomError {
    constructor() {
        super(`Name not defined!`);
    }
}

export class FindFailToComputeError extends CustomError {
    constructor() {
        super(`Find failed to compute properly`);
    }
}
