import { CustomError } from "./CustomError";

export class FingerprintNotDetected extends CustomError {
    constructor() {
        super(`Fingerprint not detected on LocalStorage`);
    }
}

export class SchemaHashNotDetected extends CustomError {
    constructor() {
        super(`Schema hash not detected on LocalStorage`);
    }
}

export class SchemaNotDetected extends CustomError {
    constructor() {
        super(`Schema not detected on LocalStorage`);
    }
}

export class SchemaModelNamesNotDetected extends CustomError {
    constructor() {
        super(`Schema model not detected on LocalStorage`);
    }
}
