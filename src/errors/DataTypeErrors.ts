import { CustomError } from "./CustomError";

export class InvalidNumber extends CustomError {
    constructor() {
        super(`Invalid Number given`);
    }
}
