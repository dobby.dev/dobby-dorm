import { CustomError } from "./CustomError";

export class NoModelSchemaFoundForName extends CustomError {
    constructor(methodName: string) {
        super(`No ModelSchema found for name: "${methodName}"`);
    }
}

export class ReservedKeywordUsedAsAttribute extends CustomError {
    constructor(attributeName: string) {
        super(
            `You can not use the attribute name "${attributeName}". It is reserved for the system`
        );
    }
}
