global.console = {
    ...console,

    log: jest.fn(),
    debug: jest.fn(),
    info: jest.fn(),
    group: jest.fn(),
    groupCollapsed: jest.fn(),
    // warn: jest.fn(),
    // error: jest.fn(),
};
