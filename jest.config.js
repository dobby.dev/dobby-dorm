require("jsdom-global")();

console.info("\n");
console.info("⚠️  OVERRIDING CONSOLE METHODS  ⚠️");
console.info("\n\n");

module.exports = {
    preset: "ts-jest",
    transform: {
        "^.+\\.[jt]s?$": "ts-jest",
    },
    testEnvironment: "jsdom",
    testMatch: ["**/tests/**/*.test.[jt]s?(x)"],
    transformIgnorePatterns: ["node_modules/(?!dobby-talks|crypto-es|uuid)"],
    setupFilesAfterEnv: ["<rootDir>/test.unit.setup.js"],
};
