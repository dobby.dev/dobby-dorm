import dts from "rollup-plugin-dts";
import esbuild from "rollup-plugin-esbuild";
import copy from "rollup-plugin-copy";
import json from "@rollup/plugin-json";

const bundle = (config) => ({
    ...config,
    input: "src/index.ts",
});

const packageJsonTarget = {
    src: "package.json",
    dest: "dist/",
    rename: "package-info.json",
};

export default [
    bundle({
        plugins: [
            esbuild(),
            copy({
                targets: [
                    {
                        src: "package.json",
                        dest: "dist/",
                        rename: "package-info.json",
                    },
                ],
            }),
            json(),
        ],
        output: [
            {
                file: `dist/dobby-dorm.js`,
                format: "cjs",
                sourcemap: true,
            },
            {
                file: `dist/dobby-dorm.mjs`,
                format: "es",
                sourcemap: true,
            },
        ],
    }),
    bundle({
        plugins: [dts(), json()],
        output: {
            file: `dist/dobby-dorm.d.ts`,
            format: "es",
        },
    }),
];
