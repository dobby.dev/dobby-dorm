export function setupLocalStorageMock(): void {
    let store = {};
    const mock = {
        getItem: function (key) {
            return store[key];
        },
        setItem: function (key, value) {
            store[key] = value.toString();
        },
        clear: function () {
            store = {};
        },
        removeItem: function (key) {
            // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
            delete store[key];
        },
    };

    Object.defineProperty(window, "localStorage", { value: mock });
}
