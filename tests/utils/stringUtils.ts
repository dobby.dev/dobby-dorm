export const formatWithSpecialCharacters =
    /[`!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?~]/;

/**
 * NOTE:
 * Removes forced json format endings.
 *
 * @param raw
 */
export function parseStorageOutput(raw: string): string {
    return raw.replace(/['"]+/g, "");
}
