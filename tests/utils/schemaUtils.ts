import { ModelSchemaClass } from "../../src";

/**
 * NOTE:
 * Returns a list of dummy schamas.
 */
export function getSchemaList(): any {
    const modelFoo = new ModelSchemaClass("foo", [
        { name: "foo_att_b", type: "integer" },
        { name: "foo_att_a", type: "date" },
    ]);

    const modelBar = new ModelSchemaClass("bar", [
        { name: "bar_att_a", type: "integer" },
        { name: "bar_att_b", type: "string" },
    ]);

    const modelBarFoo = new ModelSchemaClass("bar", [
        { name: "barFoo_att_a", type: "integer" },
        { name: "barFoo_att_b", type: "string" },
    ]);

    const modelBarBar = new ModelSchemaClass("bar", [
        { name: "barBar_att_a", type: "integer" },
        { name: "barBar_att_b", type: "string" },
    ]);

    const modelFooFoo = new ModelSchemaClass("bar", [
        { name: "FooFoo_att_a", type: "integer" },
        { name: "FooFoo_att_b", type: "string" },
    ]);

    return [modelFoo, modelBar, modelBarBar, modelFooFoo, modelBarFoo];
}
