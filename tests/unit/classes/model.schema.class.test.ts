import { ModelSchemaClass } from "../../../src";
import { ModelAttributeClass } from "../../../src/classes/models/ModelAttribute.class";
import * as stream from "stream";

const modelSchemaList = [
    { name: "attA", type: "number" },
    { name: "attB", type: "string" },
    { name: "attC", type: "string" },
    { name: "attD", type: "string" },
    { name: "attE", type: "string" },
    { name: "attF", type: "string" },
    { name: "attG", type: "string" },
    { name: "attH", type: "string" },
    { name: "attI", type: "string" },
    { name: "attJ", type: "string" },
] as ModelAttributeType[];

describe(ModelSchemaClass, () => {
    it("should be defined", function () {
        expect(ModelSchemaClass).toBeDefined();
    });

    it("should be able to instantiate the class", function () {
        const modelSchemaClass = new ModelSchemaClass(
            "testCase",
            modelSchemaList
        );

        expect(modelSchemaClass).toBeDefined();
    });
});

const modelSchemaClassTest = new ModelSchemaClass("testCase", modelSchemaList);

describe(modelSchemaClassTest.constructor, () => {
    it("should have a valid name prop", function () {
        expect(modelSchemaClassTest.name).toBeDefined();
    });

    it("should have a non empty name", function () {
        expect(modelSchemaClassTest.name.length).toBeGreaterThan(0);
    });

    it("should have attributes", function () {
        expect(modelSchemaClassTest.attributes).toBeDefined();
    });

    modelSchemaClassTest.attributes.forEach((att) => {
        it("should be defined", function () {
            expect(att).toBeDefined();
        });

        it("should be valid instance of attr", function () {
            expect(att).toBeInstanceOf(ModelAttributeClass);
        });

        it("should have a type", function () {
            expect(att.type).toBeDefined();
        });

        it("should have a valid type", function () {
            // todo improve this
            expect(att.type.length).toBeGreaterThan(0);
        });

        it("should have a name", function () {
            expect(att.name).toBeDefined();
        });

        it("should have a valid name", function () {
            expect(att.name.length).toBeGreaterThan(0);
        });

        it("should not be using a forbidden keyword", function () {
            const invalidNames = ModelAttributeClass.invalidAttributeNames();
            expect(invalidNames.includes(att.name)).toBeFalsy();
        });
    });
});
