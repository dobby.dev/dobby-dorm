import { DobbyStorageClass } from "../../../../src/classes/storage/DobbyStorage.class";
import { DobbyFreeStorageClass } from "../../../../src/classes/storage/DobbyFreeStorage.class";

describe(DobbyStorageClass, () => {
    const dobbyStorageClass = new DobbyStorageClass();

    it("should be defined", function () {
        expect(dobbyStorageClass).toBeDefined();
    });

    it("should be instance of DobbyFreeStorageClass", function () {
        expect(dobbyStorageClass).toBeInstanceOf(DobbyFreeStorageClass);
    });

    it("should have string for usedSpace", function () {
        expect(dobbyStorageClass.usedSpaceString).toBeDefined();
    });

    it("should have valid string for usedSpace", function () {
        expect(dobbyStorageClass.usedSpaceString.length).toBeGreaterThan(0);
    });

    it("should have string for free space", function () {
        expect(dobbyStorageClass.freeSpaceString).toBeDefined();
    });

    it("should have valid string for free space", function () {
        expect(dobbyStorageClass.freeSpaceString.length).toBeGreaterThan(0);
    });

    it("should have string for total space", function () {
        expect(dobbyStorageClass.totalSpaceString).toBeDefined();
    });

    it("should have valid string for total space", function () {
        expect(dobbyStorageClass.totalSpaceString.length).toBeGreaterThan(0);
    });
});
