import { DobbyFreeStorageClass } from "../../../../src/classes/storage/DobbyFreeStorage.class";

describe(DobbyFreeStorageClass, () => {
    const dobbyFreeStorageClass = new DobbyFreeStorageClass();

    it("should be defined", function () {
        expect(dobbyFreeStorageClass).toBeDefined();
    });

    it("should be instance of DobbyFreeStorage", function () {
        expect(dobbyFreeStorageClass).toBeInstanceOf(DobbyFreeStorageClass);
    });

    it("should have .freeStorage property defined", function () {
        expect(dobbyFreeStorageClass.freeStorage).toBeDefined();
    });

    it("should have .usedStorage property defined", function () {
        expect(dobbyFreeStorageClass.usedStorage).toBeDefined();
    });

    it("should have .totalStorage property defined", function () {
        expect(dobbyFreeStorageClass.totalStorage).toBeDefined();
    });

    it("should have available memory", function () {
        expect(dobbyFreeStorageClass.freeStorage).toBeGreaterThan(0);
    });

    it("should have available totalStorage", function () {
        expect(dobbyFreeStorageClass.totalStorage).toBeGreaterThan(0);
    });

    it("should have .usedStorage number", function () {
        expect(dobbyFreeStorageClass.usedStorage).not.toBeNaN();
    });

    it("should have .usedStorage greater than -1", function () {
        expect(dobbyFreeStorageClass.usedStorage).toBeGreaterThan(-1);
    });
});
