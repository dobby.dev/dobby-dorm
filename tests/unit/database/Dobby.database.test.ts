import { DobbyDatabase } from "../../../src/database/Dobby.database";
import { DobbyStorageClass } from "../../../src/classes/storage/DobbyStorage.class";
import { DobbyFreeStorageClass } from "../../../src/classes/storage/DobbyFreeStorage.class";
import {
    dbFingerprintLsKey,
    generateDbFingerprint,
} from "../../../src/utils/database.utils";

const dobbyDatabase = new DobbyDatabase();
describe(dobbyDatabase.constructor, () => {
    it("should be defined", function () {
        expect(DobbyDatabase).toBeDefined();
    });

    it("should be able to init", function () {
        expect(dobbyDatabase).toBeDefined();
    });

    it("should inherit storage class", function () {
        expect(dobbyDatabase).toBeInstanceOf(DobbyStorageClass);
    });

    it("should inherit free storage class", function () {
        expect(dobbyDatabase).toBeInstanceOf(DobbyFreeStorageClass);
    });

    it("should have a fingerprint", function () {
        expect(dobbyDatabase.fingerprint).toBeDefined();
    });

    it("should have a not blank fingerprint", function () {
        expect(dobbyDatabase.fingerprint.length).toBeGreaterThan(0);
    });

    it("should have a valid fingerprint", function () {
        expect(dobbyDatabase.fingerprint).toBe(generateDbFingerprint());
    });

    it("should have a fingerprintKey", function () {
        expect(dobbyDatabase.fingerprintKey).toBeDefined();
    });

    it("should have a valid fingerprintKey", function () {
        expect(dobbyDatabase.fingerprintKey).toBe(dbFingerprintLsKey());
    });

    it("should read ls fingerprint", function () {
        expect(dobbyDatabase.readFingerprint()).toBeDefined();
    });

    it("should read ls valid fingerprint", function () {
        expect(dobbyDatabase.readFingerprint()).toBe(dobbyDatabase.fingerprint);
    });

    it("should have print method", function () {
        expect(dobbyDatabase.size).toBeDefined();
    });

    it("should call size method without problems", function () {
        // eslint-disable-next-line @typescript-eslint/no-confusing-void-expression
        expect(dobbyDatabase.size()).toBe(undefined);
    });
});
