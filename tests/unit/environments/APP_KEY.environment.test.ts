import { env } from "../../../env.environment";
import { formatWithSpecialCharacters } from "../../utils/stringUtils";

const appKey = env.APP_KEY;

describe(appKey, () => {
    it("should be defined", function () {
        expect(appKey).toBeDefined();
    });

    it("should have length", function () {
        expect(appKey.length).toBeGreaterThan(0);
    });

    it("should be bigger than 31characters", function () {
        expect(appKey.length).toBeGreaterThan(31);
    });

    it("should contain special characters", function () {
        expect(formatWithSpecialCharacters.test(appKey)).toBeTruthy();
    });
});
