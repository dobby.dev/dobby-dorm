import { env } from "../../../env.environment";

describe(env.DB_VERSION, () => {
    const dbVersion = env.DB_VERSION;
    const majorIndex = 0;
    const minorIndex = 1;
    const patchIndex = 2;

    it("should exist", function () {
        expect(dbVersion).toBeDefined();
    });

    it("should not be blank", function () {
        expect(dbVersion.length).toBeGreaterThan(0);
    });

    it("should be possible to parse into Major Minor Path", function () {
        const split = dbVersion.split(".");
        expect(split[majorIndex]).toBeDefined();
        expect(split[minorIndex]).toBeDefined();
        expect(split[patchIndex]).toBeDefined();
    });

    it("should have valid major", function () {
        const split = dbVersion.split(".");
        expect(Number(split[majorIndex])).toBeGreaterThan(-1);
    });

    it("should have valid minor", function () {
        const split = dbVersion.split(".");
        expect(Number(split[minorIndex])).toBeGreaterThan(-1);
    });

    it("should have valid patch", function () {
        const split = dbVersion.split(".");
        expect(Number(split[patchIndex])).toBeGreaterThan(-1);
    });
});
