import { env } from "../../../env.environment";

describe("ENV", () => {
    it("should be defined", function () {
        expect(env).toBeDefined();
    });

    it("should be a valid object", function () {
        expect(env).toBeInstanceOf(Object);
    });

    it("should have some keys on it", function () {
        expect(Object.keys(env).length).toBeGreaterThan(0);
    });
});
