import { env } from "../../../env.environment";

describe(env.APP_ENV, () => {
    const data = env.APP_ENV;

    it("should exist", function () {
        expect(data).toBeDefined();
    });

    it("should be valid", function () {
        const expectedEnvs = ["development", "test", "staging", "production"];
        expect(expectedEnvs.includes(data)).toBeTruthy();
    });
});
