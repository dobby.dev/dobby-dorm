import { env } from "../../../env.environment";
import { formatWithSpecialCharacters } from "../../utils/stringUtils";

const appSalt = env.APP_SALT;

describe(appSalt, () => {
    it("should exist", function () {
        expect(appSalt).toBeDefined();
    });

    it("should not be blank", function () {
        expect(appSalt.length).toBeGreaterThan(0);
    });

    it("should be bigger than 3 characters", function () {
        expect(appSalt.length).toBeGreaterThan(3);
    });

    it("should have special characters", function () {
        expect(formatWithSpecialCharacters.test(appSalt)).toBeTruthy();
    });
});
