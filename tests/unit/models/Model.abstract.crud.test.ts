import { DobbyDorm, ModelClass, ModelSchemaClass } from "../../../src";
import { setupLocalStorageMock } from "../../utils/localStorage.utils";

setupLocalStorageMock();

// Init Model Schemas
const modelFoo = new ModelSchemaClass("foo", [
    { name: "att_a", type: "integer" },
    { name: "att_b", type: "date" },
]);
const modelBar = new ModelSchemaClass("bar", [
    { name: "att_a", type: "integer" },
    { name: "att_b", type: "string" },
]);

const modelFooBar = new ModelSchemaClass("fooBar", [
    { name: "att_a", type: "integer" },
    { name: "att_b", type: "string" },
]);

const modelFooFoo = new ModelSchemaClass("fooFoo", [
    { name: "att_a", type: "integer" },
    { name: "att_b", type: "string" },
]);

const modelBarBar = new ModelSchemaClass("barBar", [
    { name: "att_a", type: "integer" },
    { name: "att_b", type: "string" },
]);

// init ORM
const dorm = new DobbyDorm([
    modelFoo,
    modelBar,
    modelFooFoo,
    modelBarBar,
    modelFooBar,
]);

void dorm;

const models = ["foo", "bar", "fooBar", "fooFoo", "barBar"];

describe(ModelClass.find, () => {
    models.forEach((modelName) => {
        const modelTest = new ModelClass(modelName, "test-dummy-id");

        it("should retrieve instance", function () {
            const entity = ModelClass.find(modelName, modelTest.id as string);
            expect(entity).toBeDefined();
        });

        it("should retrieve valid instance", function () {
            const entity = ModelClass.find(modelName, modelTest.id as string);
            expect(entity).toBeInstanceOf(ModelClass);
        });

        it("should retrieve null on no find", function () {
            const entity = ModelClass.find(modelName, "test-dummy-id-fail");
            expect(entity).toBeNull();
        });
    });
});
