import { DobbyDorm, ModelSchemaClass, ModelClass } from "../../../src";
import { setupLocalStorageMock } from "../../utils/localStorage.utils";
import { parseStorageOutput } from "../../utils/stringUtils";

setupLocalStorageMock();

// Init Model Schemas
const modelFoo = new ModelSchemaClass("foo", [
    { name: "att_a", type: "integer" },
    { name: "att_b", type: "date" },
]);
const modelBar = new ModelSchemaClass("bar", [
    { name: "att_a", type: "integer" },
    { name: "att_b", type: "string" },
]);

const modelFooBar = new ModelSchemaClass("fooBar", [
    { name: "att_a", type: "integer" },
    { name: "att_b", type: "string" },
]);

const modelFooFoo = new ModelSchemaClass("fooFoo", [
    { name: "att_a", type: "integer" },
    { name: "att_b", type: "string" },
]);

const modelBarBar = new ModelSchemaClass("barBar", [
    { name: "att_a", type: "integer" },
    { name: "att_b", type: "string" },
]);

// init ORM
const dorm = new DobbyDorm([
    modelFoo,
    modelBar,
    modelFooFoo,
    modelBarBar,
    modelFooBar,
]);

const fooModel = new ModelClass("foo", "test-dummy-id");
const barModel = new ModelClass("bar", "test-dummy-id");
const fooBarModel = new ModelClass("fooBar", "test-dummy-id");
const fooFooModel = new ModelClass("fooFoo", "test-dummy-id");
const barBarModel = new ModelClass("barBar", "test-dummy-id");

const models = [fooModel, barModel, fooBarModel, fooFooModel, barBarModel];

describe("models", () => {
    models.forEach((model) => {
        const dataDummy = "testData";

        it("should be defined", function () {
            expect(model).toBeDefined();
        });

        it("should be a model class instance", function () {
            expect(model).toBeInstanceOf(ModelClass);
        });

        it("should store data", function () {
            expect(model.set("att_a", dataDummy)).toBeDefined();
        });

        it("should retrieve the stored data and match", function () {
            expect(parseStorageOutput(model.get("att_a") as string)).toBe(
                dataDummy.toString()
            );

            expect(model.get("att_a") !== null).toBeTruthy();
        });

        it("should update and test again", function () {
            const newData = "new dummy data for testing";
            model.set("att_a", newData);

            const savedData = model.get("att_a") as string;

            expect(parseStorageOutput(savedData)).toBe(newData);
        });
    });
});
