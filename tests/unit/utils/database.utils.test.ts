import {
    dbFingerprintLsKey,
    generateDbFingerprint,
} from "../../../src/utils/database.utils";
import { env } from "../../../env.environment";

describe(generateDbFingerprint, () => {
    it("should be defined", function () {
        expect(generateDbFingerprint).toBeDefined();
    });

    it("should return a string", function () {
        expect(typeof generateDbFingerprint()).toBe("string");
    });

    it("should return a string not blank", function () {
        expect(generateDbFingerprint().length).toBeGreaterThan(0);
    });

    it("should match this", function () {
        expect(generateDbFingerprint()).toBe(`dDORM-${env.DB_VERSION}`);
    });
});

describe(dbFingerprintLsKey, () => {
    it("should be defined", function () {
        expect(dbFingerprintLsKey).toBeDefined();
    });

    it("should return a string", function () {
        expect(typeof dbFingerprintLsKey()).toBe("string");
    });

    it("should return a populated string", function () {
        expect(dbFingerprintLsKey().length).toBeGreaterThan(0);
    });
});
