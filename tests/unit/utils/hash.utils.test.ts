import { md5 } from "../../../src/utils/hash.utils";

describe(md5, () => {
    it("should be defined", function () {
        expect(md5).toBeDefined();
    });

    it("should retrieve string", function () {
        expect(typeof md5("foo")).toBe("string");
    });

    it("should retrieve 32 char string", function () {
        expect(md5("foo").length).toBe(32);
    });
});
