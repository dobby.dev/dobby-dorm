import { downloadDB } from "../../../src/utils/download.utils";

describe(downloadDB, () => {
    it("should be defined", function () {
        expect(downloadDB).toBeDefined();
    });

    it("should be executable", function () {
        // eslint-disable-next-line @typescript-eslint/no-confusing-void-expression
        expect(downloadDB()).toBe(undefined);
    });
});
