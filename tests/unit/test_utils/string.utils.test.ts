import { formatWithSpecialCharacters } from "../../utils/stringUtils";

/**
 * NOTE:
 * Describe elements
 */
describe("formatWithSpecialCharacters", () => {
    it("should be defined", function () {
        expect(formatWithSpecialCharacters).toBeDefined();
    });

    it("should be a valid regex", function () {
        expect(formatWithSpecialCharacters).toBeInstanceOf(RegExp);
    });
});

/**
 * NOTE:
 * Describe outputs
 */
describe("formatWithSpecialCharacters output", () => {
    it('should pass on string "!!  asdasd sadasd!!"', function () {
        expect(
            formatWithSpecialCharacters.test("!! asdasd dsadas !!")
        ).toBeTruthy();
    });

    it("should fail on string asdasd", function () {
        expect(formatWithSpecialCharacters.test("asdasd")).toBeFalsy();
    });
});
