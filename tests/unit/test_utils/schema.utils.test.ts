import { getSchemaList } from "../../utils/schemaUtils";
import { ModelSchemaClass } from "../../../src";

/**
 * NOTE:
 * Describe methods
 */
describe(getSchemaList, () => {
    it("should be defined", function () {
        expect(getSchemaList).toBeDefined();
    });
});

/**
 * NOTE:
 * Describe outputs
 */
describe("schemaList", () => {
    const schemaList = getSchemaList();

    it("should be callable", function () {
        expect(schemaList).toBeDefined();
    });

    it("should retrieve a populated schema list", function () {
        expect(schemaList.length).toBeGreaterThan(0);
    });

    schemaList.forEach((schema) => {
        it("should be defined", function () {
            expect(schema).toBeDefined();
        });

        it("should be a valid instance of schema", function () {
            expect(schema).toBeInstanceOf(ModelSchemaClass);
        });
    });
});
