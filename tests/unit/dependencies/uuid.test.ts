import v4 from "../../../node_modules/uuid/dist/esm-browser/v4";

describe(v4, () => {
    it("should be defined", function () {
        expect(v4).toBeDefined();
    });

    it("should retrieve a string", function () {
        expect(typeof v4()).toBe("string");
    });

    it("should have some length", function () {
        expect(v4().toString().length).toBeGreaterThan(5);
    });
});
