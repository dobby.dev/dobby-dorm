import DobbyTalks from "../../../node_modules/dobby-talks/src/index";

describe(DobbyTalks, () => {
    it("should be defined", function () {
        expect(DobbyTalks).toBeDefined();
    });

    it("should be init without problems", function () {
        const dobby = new DobbyTalks();
        expect(dobby).toBeDefined();
    });

    it("should init as instance", function () {
        const dobby = new DobbyTalks();
        expect(dobby).toBeInstanceOf(DobbyTalks);
    });
});
