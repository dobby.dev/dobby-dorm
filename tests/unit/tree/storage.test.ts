import { Storage } from "../../../src/tree/Storage";

describe(Storage, () => {
    const storage = new Storage();

    it("should be defined", function () {
        expect(storage).toBeDefined();
    });

    it("should have defined used storage", function () {
        expect(storage.usedStorage).toBeDefined();
    });

    it("should have defined free storage", function () {
        expect(storage.freeStorage).toBeDefined();
    });

    it("should have defined total storage", function () {
        expect(storage.totalStorage).toBeDefined();
    });

    it("should have valid used storage", function () {
        expect(storage.usedStorage).toBeGreaterThan(-1);
    });

    it("should have valid free storage", function () {
        expect(storage.freeStorage).toBeGreaterThan(-1);
    });

    it("should have free storage", function () {
        expect(storage.freeStorage).toBeGreaterThan(0);
    });

    it("should have valid total storage", function () {
        expect(storage.totalStorage).toBeGreaterThan(-1);
    });

    it("should have available total storage", function () {
        expect(storage.totalStorage).toBeGreaterThan(0);
    });
});
