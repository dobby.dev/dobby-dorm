import { Logger } from "../../../src/tree/Logger";

describe(Logger, () => {
    const logger = new Logger();

    it("should be defined", function () {
        expect(logger).toBeDefined();
    });

    it("should have a logger prop", function () {
        expect(logger.logger).toBeDefined();
    });

    it("should work without error", function () {
        expect(logger.logger.talk("test", "test", false)).toBeUndefined();
    });

    it("should have a version", function () {
        expect(logger.version).toBeDefined();
    });

    it("should have a valid version", function () {
        expect(logger.version.length).toBeGreaterThan(0);
    });
});
