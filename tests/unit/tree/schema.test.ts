import { Schema } from "../../../src/tree/Schema";
import { getSchemaList } from "../../utils/schemaUtils";
import { ModelSchemaClass } from "../../../src";
import { ModelAttributeClass } from "../../../src/classes/models/ModelAttribute.class";

describe("Schema", () => {
    const schema = new Schema(getSchemaList());

    it("should be defined", function () {
        expect(schema).toBeDefined();
    });

    it("should have array of models", function () {
        expect(schema.models).toBeDefined();
        expect(schema.models).toBeInstanceOf(Array);
    });

    it("should have valid array of models", function () {
        expect(schema.models.length).toEqual(getSchemaList().length);
    });

    it("should read a schemaHash", function () {
        expect(schema.readSchemaHash()).toBeDefined();
    });

    it("should read model names", function () {
        expect(schema.readModelNames()).toBeDefined();
    });

    it("should have more than zero", function () {
        expect(schema.readModelNames().length).toBeGreaterThan(0);
    });

    it("should read a non blank schema", function () {
        expect(schema.readSchemaHash().length).toBeGreaterThan(0);
    });

    it("should read a md5 schema", function () {
        expect(schema.readSchemaHash().length).toBe(32);
    });

    schema.models.forEach((model) => {
        it("should be a defined model", function () {
            expect(model).toBeDefined();
        });

        it("should be a valid instance of models", function () {
            expect(model).toBeInstanceOf(ModelSchemaClass);
        });

        it("should get the right Schema By name", function () {
            const modelSchema = schema.getSchemaByName(model.name);
            expect(modelSchema).toBeDefined();
        });

        it("should get name for every schema", function () {
            const modelSchema = schema.getSchemaByName(model.name);
            expect(modelSchema.name).toBeDefined();
        });

        it("should get attributes for every schema", function () {
            const modelSchema = schema.getSchemaByName(model.name);
            expect(modelSchema.attributes).toBeDefined();
        });

        it("should should get a list of attributes", function () {
            const modelSchema = schema.getSchemaByName(model.name);
            expect(modelSchema.attributes).toBeInstanceOf(Array);
        });

        it("should have valid attributes", function () {
            const modelSchema = schema.getSchemaByName(model.name);
            modelSchema.attributes.forEach((att) => {
                expect(att).toBeInstanceOf(ModelAttributeClass);
            });
        });

        const modelSchema = schema.getSchemaByName(model.name);
        modelSchema.attributes.forEach((att) => {
            it("should have valid attribute name for every attribute", function () {
                expect(att.name).toBeDefined();
            });

            it("should have valid attribute type for every attribute", function () {
                expect(att.type).toBeDefined();
            });

            it("should not have any invalid attribute", function () {
                expect(
                    ModelAttributeClass.invalidAttributeNames().includes(
                        att.type
                    )
                ).not.toBeTruthy();
            });
        });
    });
});
