import { FasterServiceWorker } from "../../../src/workers/FasterService.worker";

describe(FasterServiceWorker, () => {
    it("should be defined", function () {
        expect(FasterServiceWorker).toBeDefined();
    });

    it("should have a constructor", function () {
        expect(FasterServiceWorker.constructor).toBeDefined();
    });
});
