import { BaseWorker } from "../../../src/workers/base.worker";

describe(BaseWorker, () => {
    it("should be defined", function () {
        expect(BaseWorker).toBeDefined();
    });

    it("should have a constructor", function () {
        expect(BaseWorker.constructor).toBeDefined();
    });
});
