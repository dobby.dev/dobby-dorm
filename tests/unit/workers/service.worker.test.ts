import { ServiceWorker } from "../../../src/workers/service.worker";

describe(ServiceWorker, () => {
    it("should be defined", function () {
        expect(ServiceWorker).toBeDefined();
    });

    it("should have a constructor", function () {
        expect(ServiceWorker.constructor).toBeDefined();
    });
});
