export const env = {
    /**
     * NOTE:
     * Marks application environment.
     */
    APP_ENV: "development",

    /**
     * NOTE:
     * Defines database version for schema
     */
    DB_VERSION: "0.0.1",

    /**
     * NOTE:
     * Defines Application key for encryption
     */
    APP_KEY: "80y#RglqBRe1v5mG8BQ@xYpr6xu#T0kLb@8KNUYM",

    /**
     * NOTE:
     * Defines Application salt for encryption
     */
    APP_SALT: "?7tF",

    /**
     * NOTE:
     * Defines tick timer in MS for ServiceWorker
     */
    SERVICE_WORKER_TIMER: 10000,

    /**
     * NOTE:
     * Defines tick timer in MS for FasterServiceWorker
     */
    FASTER_SERVICE_WORKER_TIMER: 1234,
};
