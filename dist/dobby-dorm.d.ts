import DobbyTalks from '../../node_modules/dobby-talks/src/index';

/**
 * NOTE:
 * Contains ModelAttribute type, name and can contain value.
 * Sustains computational necessities of class.
 */
declare class ModelAttributeClass {
    name: string;
    type: string;
    value?: ModelAttributeValueType;
    /**
     * NOTE:
     * Returns a list with invalid model attribute names.
     * Keywords reserved for system.
     */
    static invalidAttributeNames(): string[];
    constructor(name: string, type: string, value?: ModelAttributeValueType);
    /**
     * NOTE:
     * Sets value for attribute from model.
     *
     * stores
     * @param value
     */
    setValue(value: ModelAttributeValueType): void;
}

/**
 * NOTE:
 * Sustains model name and attributes.
 * Attribute list contains attribute type and name. Can also contain value.
 */
declare class ModelSchemaClass {
    name: string;
    attributes: ModelAttributeClass[];
    schemaKey: string;
    _l: any;
    storage: Storage;
    constructor(name: string, attributes: ModelAttributeType[]);
    /**
     * NOTE:
     * Prints current modelSchema string.
     */
    readSchema(): string;
    /*********************************************************************************************************************
     * PRIVATE METHODS
     */
    /**
     * NOTE:
     * Hydrate attribute types.
     *
     * @param attrs
     * @private
     */
    private setupAttrs;
    /**
     * NOTE:
     * Validates Attribute names.
     *
     * @param attrs
     * @private
     */
    private validateAttributeNames;
}

/**
 * NOTE:
 * Handles free space calculations and predictions.
 */
declare class DobbyFreeStorageClass {
    freeStorage: number;
    totalStorage: number;
    usedStorage: number;
    constructor();
    /*********************************************************************************************************************
     * PRIVATE METHODS
     */
    /**
     * NOTE:
     * Chain actions to fill:
     * - .freeStorage <number>
     * - .totalStorage <number>
     * - .usedStorage <number>
     * @private
     */
    private calculateStorageValues;
    /**
     * NOTE:
     * Speculates available totalStorage.
     *
     * @private
     */
    private calculateTotalStorage;
    /**
     * NOTE:
     * Calculates approx measurable used storage.
     *
     * @private
     */
    private calculateUsedStorage;
    /**
     * NOTE:
     * Calculates predictable free storage.
     * Updates .freeStorage property.
     *
     * @private
     */
    private calculateFreeStorage;
}

/**
 * NOTE:
 * Handles Storage complexity.
 */
declare class DobbyStorageClass extends DobbyFreeStorageClass {
    units: string[];
    freeSpaceString: string;
    usedSpaceString: string;
    totalSpaceString: string;
    constructor();
    sizeString(size: number): string;
}

/**
 * NOTE:
 * Handles Database usage.
 */
declare class DobbyDatabase extends DobbyStorageClass {
    fingerprint: string;
    fingerprintKey: string;
    _l: any;
    constructor();
    /**
     * NOTE:
     * returns current fingerprint or throws
     *
     */
    readFingerprint(): string;
    /**
     * NOTE:
     * Alias from private printSizeNotice()
     * Logs information about space used by the DB.
     *
     * @alias printSizeNotice();
     */
    size(): void;
    /**
     * NOTE:
     * Downloads database file
     */
    backup(fileName?: string): void;
    /*******************************************************************************************************************
     * PRIVATE METHODS
     */
    /**
     * NOTE:
     * Logs information about space used by the DB.
     */
    private printSizeNotice;
    /**
     * NOTE:
     * Compares stored fingerprint with system version fingerprint.
     * @private
     */
    private handshakeFingerprint;
    /**
     * NOTE:
     * Simply update current key.
     * Useful for new entries or resets.
     * @private
     */
    private updateFingerprint;
    /**
     * NOTE:
     * Upgrade version data from previous fingerprint into new fingerprint.
     * @TODO => migrate data
     * @private
     */
    private upgradeFingerprint;
}

declare class ModelAbstractClass extends DobbyDatabase {
    id: string | null;
    schemaKey: string;
    name?: string;
    any: string;
    static find(modelName: string, idString: string): ModelClass | null;
    /**
     * NOTE:
     * Gets attribute by name
     *
     * @todo throw validation
     * @param attributeName
     */
    get(attributeName: ModelAttributeNameType): ModelAttributeValueType;
    /**
     * NOTE:
     * Sets attribute value by name and value.
     *
     * @todo throw validation
     * @param attributeName
     * @param value
     */
    set(attributeName: ModelAttributeNameType, value: ModelAttributeValueType): ModelAbstractClass;
    /*********************************************************************************************************************
     * PRIVATE METHODS
     */
    private modelAttributeKey;
}

/**
 * NOTE:
 * Handles Model properties.
 */
declare class ModelClass extends ModelAbstractClass {
    attributes: ModelSchemaType[];
    values: ModelAttributeValueType[];
    data: {
        id: any;
        any: any;
    };
    schema: ModelSchemaType[];
    /**
     * NOTE:
     * Class constructor.
     *
     * @param name
     * @param id
     */
    constructor(name: string, id?: string);
    /*********************************************************************************************************************
     * PRIVATE METHODS
     */
    /**
     * NOTE:
     * Loads schema from model name
     *
     * @private
     * @throws SchemaNotDetected when no schema can be read
     */
    private loadSchema;
    /**
     * NOTE:
     * loads object in memory.
     *
     * @private
     */
    private loadAbstractionLayer;
}

/**
 * NOTE:
 * Storage injection on DORM.
 */
declare class Storage$1 extends DobbyDatabase {
}

/**
 * NOTE:
 * Appends Log functionality to parent class
 */
declare class Logger extends Storage$1 {
    logger: DobbyTalks;
    version: string;
    constructor();
    /*********************************************************************************************************************
     * PRIVATE METHODS
     */
    /**
     * NOTE:
     * Highlights the disclaimer message.
     * @private
     */
    private printDisclaimer;
}

/**
 * Appends Model Schema and computational necessities.
 */
declare class Schema extends Logger {
    models: ModelSchemaClass[];
    modelNames: string;
    schemaKey: string;
    schemaNamesKey: string;
    constructor(schemaList: ModelSchemaClass[]);
    /**
     * NOTE:
     * Gets ModelSchema by given name.
     *
     * @param modelName
     * @return model<ModelSchemaClass>
     */
    getSchemaByName(modelName: any): ModelSchemaClass;
    /**
     * NOTE:
     * Prints stored schemaHash
     */
    readSchemaHash(): string;
    /**
     * NOTE:
     * Prints stored model names.
     */
    readModelNames(): string;
    /*******************************************************************************************************************
     * Private methods
     */
    /**
     * NOTE:
     * Validates & matches schema.
     * Updates or Writes for the first time.
     *
     * @private
     */
    private handshakeSchema;
    /**
     * NOTE:
     * Writes schema for the first time.
     * Writes schema model names
     *
     * @param jsonSchema
     * @private
     */
    private writeSchema;
    /**
     * NOTE:
     * Updates schema content. Including data.
     * Updates schema model names
     *
     * @param jsonSchema
     * @private
     */
    private updateSchema;
}

/**
 * NOTE:
 * Entry point for DobbyDorm functionality.
 */
declare class DobbyDorm extends Schema {
    constructor(args: any);
    /*******************************************************************************************************************
     * PRIVATE METHODS
     */
    /**
     * NOTE:
     * Integrates different Workers on startup.
     *
     * @private
     */
    private setupWorkers;
}

export { DobbyDorm, ModelClass, ModelSchemaClass };
